import pyglet
import engine
import resources
import entities
import random


if __name__ == '__main__':
    world, to_precache = resources.load_world('game.json')
    width, height = 1280, 720
    window = pyglet.window.Window(fullscreen=False, resizable=True,
                                  width=width, height=height, caption="TowerDefence")
    client = engine.Client(world)
    world.camera.position[2] = -18
    pyglet.clock.get_fps()
    fps_display = pyglet.clock.ClockDisplay()
    logger = engine.Logger()
    stat_logger = engine.Logger(y=650, color=(190, 95, 222, 255))
    towers = [*filter(lambda x: isinstance(x, entities.Tower),
                          world.game.rules.ents.get_entities())]

    @window.event
    def on_resize(width, height):
        client.set_viewport(width, height)
        # return pyglet.event.EVENT_HANDLED - an odd thing

    @window.event
    def on_draw():
        g = world.game
        stat_logger.clear()
        stat_logger.log('Gold: {}, Lives: {}, Wave: {}, Time: {}'
                        .format(g.gold, g.lives, g.wave, g.time))
        client.draw()
        logger.draw()
        fps_display.draw()
        stat_logger.draw()

    @window.event
    def on_mouse_motion(x, y, dx, dy):
        client.move_mouse(x, y)

    @window.event
    def on_mouse_drag(x, y, dx, dy, button, modifiers):
        client.move_mouse(x, y)
        if button == 4:
            world.camera.position[0] -= dx * 0.05
            world.camera.position[1] -= dy * 0.05

    @window.event
    def on_mouse_press(x, y, button, modifiers):
        client.move_mouse(x, y)
        if button == 1:
            x, y = world.selected_cell
            if 0 <= x < world.game.rules.width and \
                    0 <= y < world.game.rules.height:
                if world.game.tower_at((x, y)):
                    sell_tower(x, y)
                else:
                    build_tower(x, y)

    @window.event
    def on_mouse_scroll(x, y, scroll_x, scroll_y):
        world.camera.position[2] += scroll_y

    def build_tower(x, y):
        tower = random.choice(towers)
        success = world.game.build_tower((x, y), tower.type)
        tower_name = '[type={}]'.format(tower.type)
        if success:
            logger.log('Tower built at', x, y, tower_name)
        else:
            logger.log('Can\'t build tower at', x, y, tower_name)

    def sell_tower(x, y):
        success = world.game.sell_tower((x, y))
        if success:
            logger.log('Tower sold at', x, y)
        else:
            logger.log('Can\'t sell tower at', x, y)

    build_tower(1, 3)
    logger.log('Precaching')
    for x in to_precache:
        x.precache()
    logger.log(len(to_precache), 'resources have been precached')
    client.init()
    logger.log('Engine loaded')
    logger.log('RMB to move camera, WHEEL to zoom, LMB to build\sell tower')
    logger.log('First wave will arrive at:', world.game.rules.wave_interval)
    pyglet.clock.schedule(world.game.update)
    pyglet.clock.set_fps_limit(60)
    pyglet.app.run()
