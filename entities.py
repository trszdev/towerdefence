import copy


class EntFactory:
    def __init__(self):
        self._ents = []
        self.produced = 0

    def add_entity(self, ent):
        ent.type = len(self._ents)
        self._ents.append(ent)
        return self

    def create_new(self, type):
        selected = copy.deepcopy(self.get_entity(type))
        selected.id = self.produced
        self.produced += 1
        return selected

    def get_entity(self, type):
        selected = self._ents[type]
        return selected

    def get_entities(self):
        return self._ents


class Entity:
    def __init__(self):
        self.id = -1
        self.type = -1

    def __repr__(self):
        return repr(type(self)) + repr(self.__dict__)

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id


class Unit(Entity):
    def __init__(self):
        super().__init__()
        self.location = [0, 0]


class Monster(Unit):
    def __init__(self, max_hp, speed):
        super().__init__()
        self.max_hp = max_hp
        self.hp = max_hp
        self.speed = speed
        self.effects = set()
        self.waypoint = [0, 0]


class Tower(Unit):
    def __init__(self, reload_time, missile_type, attack_radius):
        super().__init__()
        self.aim = None
        self.reload_time = reload_time
        self.missile_type = missile_type
        self.attack_radius = attack_radius
        self.last_shot = 0


class Missile(Unit):
    def __init__(self, speed, effects=set()):
        super().__init__()
        self.speed = speed
        self.aim = None
        self.effects = effects


class Effect(Entity):
    def __init__(self, duration):
        super().__init__()
        self.duration = duration
        self.aim = None
        self.game = None

    def set_aim(self, aim):
        self.aim = aim
        return self

    def set_game(self, game):
        self.game = game
        return self

    def tick(self, dt):
        self.duration -= dt
