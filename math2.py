import math


def get_diffs(start, end):
    return map(lambda x: x[0] - x[1], zip(end, start))


def manhattan(start, end):
    diffs = get_diffs(start, end)
    return max(map(abs, diffs))


def euclidean(start, end):
    diffs = get_diffs(start, end)
    diffs2 = map(lambda x: x * x, diffs)
    return math.sqrt(sum(diffs2))


def clamp(value, min_bound=-1, max_bound=1):
    return min(max(min_bound, value), max_bound)


def copysign(val, sign, eps=1e-5):
    close_to_zero = abs(sign) <= eps
    if close_to_zero:
        return 0
    elif sign > 0:
        return val
    else:
        return -val


def is_in_segment(point, segment, eps=1e-5):
    ps = euclidean(point, segment[0])
    pe = euclidean(point, segment[1])
    sl = euclidean(*segment)
    return abs(sl-pe-ps) <= eps
