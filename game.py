import math2
import random


class Rules:
    def __init__(self, waves, costs, ents, route, lives=30, gold=1000,
                 sell_rate=0.75, wave_interval=10.0, width=20, height=20, speed=1):
        self.lives = lives
        self.gold = gold
        self.route = route
        self.sell_rate = sell_rate
        self.ents = ents
        self.costs = costs
        self.wave_interval = wave_interval
        self.waves = waves
        self.width = width
        self.height = height
        self.speed = speed


class Game:
    def __init__(self, rules):
        self.rules = rules
        self.gold = rules.gold
        self.lives = rules.lives
        self.wave = 0
        self.time = 0
        self.towers = set()
        self.monsters = set()
        self.missiles = set()

    def tower_at(self, pos):
        for tower in self.towers:
            if tower.location == pos:
                return tower

    def can_build(self, pos):
        if pos[0] > self.rules.width or \
                pos[1] > self.rules.height or self.tower_at(pos):
            return False
        if self.rules.route:
            last = self.rules.route[0]
            for current in self.rules.route[1:]:
                if math2.is_in_segment(pos, (last, current)):
                    return False
                last = current
            else:
                return last != pos
        return True

    def _create_unit(self, unit_type, pos):
        unit = self.rules.ents.create_new(unit_type)
        unit.location = pos
        return unit

    def build_tower(self, pos, tower_type):
        cost = self.rules.costs.get(tower_type, self.gold+1)
        if self.can_build(pos) and cost <= self.gold:
            self.gold -= cost
            tower = self._create_unit(tower_type, pos)
            self.towers.add(tower)
            return True
        return False

    def sell_tower(self, pos):
        tower = self.tower_at(pos)
        if not tower:
            return 0
        cost = self.rules.costs[tower.type] * self.rules.sell_rate
        self.gold += cost
        self.towers.remove(tower)
        return cost

    def _check_waves(self):
        max_wave = len(self.rules.waves)
        wave = min(int(self.time // self.rules.wave_interval), max_wave)
        x, y = self.rules.route[0]
        for w in range(self.wave, wave):
            t, amount = self.rules.waves[w]
            for i in range(amount):
                rnd = random.uniform(-0.5, 0.5)
                monster = self._create_unit(t, (x+rnd, y+rnd))
                monster.speed += random.uniform(-0.025, 0.025)
                self.monsters.add(monster)
        self.wave = wave

    def _update_effects(self, dt):
        for monster in self.monsters:
            to_delete = []
            for effect in monster.effects:
                if effect.duration <= 0:
                    to_delete.append(effect)
                else:
                    effect.tick(dt)
            monster.effects.difference_update(to_delete)

    def _update_monsters(self, dt):
        for monster in self.monsters:
            if monster.waypoint == monster.location:
                if monster.waypoint in self.rules.route:
                    index = self.rules.route.index(monster.waypoint) + 1
                    if not index >= len(self.rules.route):
                        monster.waypoint = self.rules.route[index]
                else:
                    monster.waypoint = self.rules.route[0]
            else:
                pdist = dt * monster.speed
                dx, dy = math2.get_diffs(monster.location, monster.waypoint)
                rdist = math2.euclidean((dx, dy), (0, 0))
                if pdist >= rdist:
                    monster.location = monster.waypoint
                else:
                    x, y = monster.location
                    dx /= rdist
                    dy /= rdist
                    monster.location = (x + dx * pdist, y + dy * pdist)

    def _check_dead_monsters(self):
        dead_monsters = []
        for monster in self.monsters:
            if monster.hp <= 0:
                self.gold += self.rules.costs[monster.type]
                dead_monsters.append(monster)
            elif monster.location == self.rules.route[-1]:
                self.lives -= 1
                dead_monsters.append(monster)
        self.monsters.difference_update(dead_monsters)

    def _missile_explode(self, missile):
        for effect_type in missile.effects:
            effect = self.rules.ents.create_new(effect_type)
            effect.set_aim(missile.aim).set_game(self).tick(dt=0)
            missile.aim.effects.add(effect)

    def _update_missiles(self, dt):
        to_delete = []
        for missile in self.missiles:
            if missile.aim in self.monsters:
                dx, dy = math2.get_diffs(missile.location, missile.aim.location)
                rdist = math2.euclidean((dx, dy), (0, 0))
                pdist = dt * missile.speed
                if pdist >= rdist:
                    self._missile_explode(missile)
                    to_delete.append(missile)
                else:
                    x, y = missile.location
                    dx /= rdist
                    dy /= rdist
                    missile.location = (x + dx * pdist, y + dy * pdist)
            else:
                to_delete.append(missile)
        self.missiles.difference_update(to_delete)

    def _update_towers(self):
        towers_with_aims = []
        for tower in self.towers:
            dist_to_tower = lambda x: math2.euclidean(tower.location, x.location)
            is_in_radius = lambda x: dist_to_tower(x) <= tower.attack_radius
            if tower.aim in self.monsters and is_in_radius(tower.aim):
                towers_with_aims.append(tower)
            else:
                aims = filter(is_in_radius, self.monsters)
                tower.aim = next(iter(aims), None)
                if tower.aim:
                    towers_with_aims.append(tower)

        for tower in towers_with_aims:
            can_shoot = tower.last_shot + tower.reload_time <= self.time
            if can_shoot and tower.aim:
                tower.last_shot = self.time
                missile = self._create_unit(tower.missile_type, tower.location)
                missile.aim = tower.aim
                self.missiles.add(missile)

    def update(self, dt):
        if self.lives <= 0:
            return
        dt *= self.rules.speed
        self._check_waves()
        self._update_effects(dt)
        self._update_monsters(dt)
        self._check_dead_monsters()
        self._update_missiles(dt)
        self._update_towers()
        self.time += dt
