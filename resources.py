import json
import game
import entities
import engine
import effects


_root = 'resources/'


def json_from_file(path):
    with open(path, 'r') as f:
        return json.loads(f.read())


def _load_frost(ent, ents, missile):
    if 'frost' in ent:
        f = ent['frost']
        f = effects.FrostEffect(f['duration'], f['value'], f['is_abs'])
        ents.add_entity(f)
        missile.effects.add(f.type)


def _load_damage(ent, ents, missile):
    if 'damage' in ent:
        d = ent['damage']
        d = effects.DamageEffect(d['value'], d['is_abs'], d['duration'])
        ents.add_entity(d)
        missile.effects.add(d.type)


def _load_tower(ent, ents, costs):
    missile = entities.Missile(ent['missile_speed'])
    ents.add_entity(missile)
    tower = entities.Tower(ent['reload_time'], missile.type, ent['attack_radius'])
    _load_frost(ent, ents, missile)
    _load_damage(ent, ents, missile)
    ents.add_entity(tower)
    costs[tower.type] = ent['cost']


def _load_monster(ent, ents, costs, waves):
    monster = entities.Monster(ent['hp'], ent['speed'])
    ents.add_entity(monster)
    costs[monster.type] = ent['cost']
    waves.append((monster.type, ent['amount']))


def load_game(filename, root=_root):
    opts = json_from_file(root + filename)
    ents = entities.EntFactory()
    costs = {}
    waves = []
    for ent in opts['ents']:
        if ent['type'] == 'tower':
            _load_tower(ent, ents, costs)
        elif ent['type'] == 'monster':
            _load_monster(ent, ents, costs, waves)
    route = [*map(tuple, opts['route'])]
    rules = game.Rules(waves, costs, ents, route, opts['lives'],
                       opts['gold'], opts['sell_rate'], opts['wave_interval'],
                       speed=opts['game_speed'])
    return game.Game(rules)


def load_world(filename, root=_root):
    game2 = load_game(filename, root)
    camera = engine.Camera()
    models = {}
    bmodels = get_builtin_models()
    to_precache = [
        bmodels['tower'],
        bmodels['tower'].texture,
        bmodels['monster'].models[0][0],
        bmodels['monster'].models[0][0].texture,
        bmodels['missile'].models[0][0],
        bmodels['missile'].models[0][0].texture,
    ]
    for ent in game2.rules.ents.get_entities():
        if isinstance(ent, entities.Tower):
            models[ent.type] = bmodels['tower']
        elif isinstance(ent, entities.Monster):
            models[ent.type] = bmodels['monster']
        elif isinstance(ent, entities.Missile):
            models[ent.type] = bmodels['missile']
    map_relief = get_map_relief(game2)
    world = engine.World(camera, models, map_relief, game2)
    return world, to_precache


def get_map_relief(g):
    grid = engine.Grid(g.rules.width, g.rules.height)
    waypoint2 = engine.Grid(1, 1, icolor=(0, 157/255, 209/255))
    waypoint = engine.Grid(1, 1, icolor=(99/255, 197/255, 230/255))
    waypoint3 = engine.Grid(1, 1, icolor=(61/255, 207/255, 1))
    result = engine.ComplexModel()
    sx, sy = g.rules.route[0]
    lx, ly = g.rules.route[-1]
    result.add_model(waypoint2, offset=(sx, sy, 0))
    result.add_model(waypoint2, offset=(lx, ly, 0))
    for x, y in g.rules.route:
        result.add_model(waypoint, offset=(x, y, 0))
        for i in range(min(sx, x), max(x, sx)):
            result.add_model(waypoint3, offset=(i, y, 0))
        for j in range(min(sy, y), max(y, sy)):
            result.add_model(waypoint3, offset=(x, j, 0))
        sx, sy = x, y
    return result.add_model(grid)


def complex_mdl(mdl, offset=(0, 0, 0), rot=(0, 0, 0), scale=1):
    s = (scale, scale, scale)
    return engine.ComplexModel().add_model(mdl, offset, rot, s)


def get_builtin_models():
    return {
        'tower': cube(_root + 'tower.png'),
        'monster': complex_mdl(
            cube(_root + 'monster.png'),
            scale=0.5,
            offset=(0.25, 0.25, 0.5),
            #rot=(180, 0, 0)
        ),
        'missile': complex_mdl(
            cube(_root + 'tnt.png'),
            scale=0.25,
            offset=(0.25, 0.25, 0.25)
        ),
    }


def cube(texture_path):
    return ortho_ppiped(texture_path, 1, 1, 1)


def ortho_ppiped(texture_path, a, b, c):
    verts = [
        0, 0, 0,   a, b, 0,  a, 0, 0, #
        0, 0, 0,   0, b, 0,  a, b, 0, #

        0, 0, c,   a, 0, c,  a, b, c,
        0, 0, c,   a, b, c,  0, b, c,

        0, 0, 0,   a, 0, c,  0, 0, c, #
        0, 0, 0,   a, 0, 0,  a, 0, c, #

        0, b, 0,   0, b, c,  a, b, c,
        0, b, 0,   a, b, c,  a, b, 0,

        0, 0, 0,   0, b, c,  0, b, 0, #
        0, 0, 0,   0, 0, c,  0, b, c, #

        a, 0, 0,   a, b, 0,  a, b, c,
        a, 0, 0,   a, b, c,  a, 0, c,
    ]
    tcoords = [
        0, 0,  1, 1,  0, 1,
        0, 0,  1, 0,  1, 1,
        0, 0,  0, 1,  1, 1,
        0, 0,  1, 1,  1, 0,
    ] * 6
    texture = engine.load_texture(texture_path)
    return engine.TextureModel(verts, tcoords, texture)
