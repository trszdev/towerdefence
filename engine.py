import itertools
from entities import Entity
from pyglet.gl import *
from PIL import Image
from ctypes import pointer
import pyglet


def vec(type, *args):
    ctor = (type * len(args))
    return ctor(*args)


class Model(Entity):
    def precache(self):
        pass

    def draw(self, *args):
        pass


def load_texture(path):
    return Texture(Image.open(path))


class Texture:
    def __init__(self, img):
        self.img = img.convert('RGBA')
        self.width, self.height = img.size

    def precache(self):
        self.link = GLuint()
        glGenTextures(1, pointer(self.link))
        glBindTexture(GL_TEXTURE_2D, self.link)
        pixels = itertools.chain(*self.img.getdata())
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, self.width, self.height,
                     0, GL_RGBA, GL_UNSIGNED_BYTE, vec(GLboolean, *pixels))
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)


class ComplexModel(Model):
    def __init__(self):
        super().__init__()
        self.models = []

    def add_model(self, mdl, offset=(0, 0, 0), rot=(0, 0, 0), scale=(1, 1, 1)):
        self.models.append((mdl, offset, rot, scale))
        return self

    def draw(self, *args):
        for mdl, offset, rot, scale in self.models:
            glPushMatrix()
            glTranslatef(*offset)
            glRotatef(rot[0], 1, 0, 0)
            glRotatef(rot[1], 0, 1, 0)
            glRotatef(rot[2], 0, 0, 1)
            glScalef(*scale)
            mdl.draw(*args)
            glPopMatrix()


class Grid(Model):
    def __init__(self, width, height, color=(1, 0, 0), icolor=(1, 1, 0)):
        super().__init__()
        self.width = width
        self.height = height
        self.color = color
        self.icolor = icolor

    def draw(self):
        glColor3f(*self.color)
        glBegin(GL_LINES)
        for i in range(self.width+1):
            glVertex3f(i, 0, 0)
            glVertex3f(i, self.height, 0)
        for j in range(self.height+1):
            glVertex3f(0, j, 0)
            glVertex3f(self.width, j, 0)
        glEnd()
        glColor3f(*self.icolor)
        glBegin(GL_QUADS)
        glVertex3f(0, 0, 0)
        glVertex3f(self.width, 0, 0)
        glVertex3f(self.width, self.height, 0)
        glVertex3f(0, self.height, 0)
        glEnd()
        glColor3f(1, 1, 1)


class TextureModel(Model):
    def __init__(self, vertices, text_coords, texture):
        super().__init__()
        self.n = len(vertices)
        self.vertices = vertices
        self.text_coords = text_coords
        self.texture = texture

    def draw(self):
        glEnableClientState(GL_VERTEX_ARRAY)
        glBindBuffer(GL_ARRAY_BUFFER, self.vlink)
        glVertexPointer(3, GL_FLOAT, 0, None)
        glEnableClientState(GL_TEXTURE_COORD_ARRAY)
        glBindBuffer(GL_ARRAY_BUFFER, self.tlink)
        glTexCoordPointer(2, GL_FLOAT, 0, None)
        glBindTexture(GL_TEXTURE_2D, self.texture.link)
        glEnable(GL_TEXTURE_2D)
        #glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,  GL_MODULATE)
        glDrawArrays(GL_TRIANGLES, 0, self.n // 3)
        glDisable(GL_TEXTURE_2D)
        glDisableClientState(GL_VERTEX_ARRAY)
        glDisableClientState(GL_TEXTURE_COORD_ARRAY)

    def precache(self):
        self.vlink = GLuint()
        glGenBuffers(1, pointer(self.vlink))
        self.tlink = GLuint()
        glGenBuffers(1, pointer(self.tlink))
        glBindBuffer(GL_ARRAY_BUFFER, self.vlink)
        glBufferData(GL_ARRAY_BUFFER, self.n * 4, vec(GLfloat, *self.vertices), GL_STATIC_DRAW)
        glBindBuffer(GL_ARRAY_BUFFER, self.tlink)
        glBufferData(GL_ARRAY_BUFFER, self.n * 4, vec(GLfloat, *self.text_coords), GL_STATIC_DRAW)


class Camera:
    def __init__(self):
        self.position = [0, 0, 0]
        self.rotation = [0, 0, 0]


class World:
    def __init__(self, camera, models, map_relief, game):
        self.rotations = {}
        self.models = models
        self.camera = camera
        self.map_relief = map_relief
        self.game = game
        self.selected_cell = (-1, -1)

    def draw_unit(self, unit):
        glPushMatrix()
        mdl = self.models[unit.type]
        x, y = unit.location
        glTranslatef(x, y, 0)
        mdl.draw()
        glPopMatrix()

    def do_camera_routine(self):
        glRotatef(self.camera.rotation[0], 1, 0, 0)
        glRotatef(self.camera.rotation[1], 0, 1, 0)
        glRotatef(self.camera.rotation[2], 0, 0, 1)
        glTranslatef(*self.camera.position)

    def draw(self):
        self.do_camera_routine()
        self.map_relief.draw()
        for monster in self.game.monsters:
            # draw hps
            self.draw_unit(monster)
        for tower in self.game.towers:
            if tower.location == self.selected_cell:
                glColor4f(1, 0, 0, 1)
                self.draw_unit(tower)
                glColor4f(1, 1, 1, 1)
            else:
                self.draw_unit(tower)
        for missile in self.game.missiles:
            self.draw_unit(missile)


class Logger:
    def __init__(self, color=(156, 20, 20, 255),
                 font_size=14, x=0, y=600, width=400, height=200, symb_limit=300):
        self._label = pyglet.text.Label('',
                          font_name='Courier',
                          color=color,
                          font_size=font_size,
                          multiline=True,
                          width=width,
                          height=height,
                          x=x, y=y,
                          anchor_x='left', anchor_y='top')
        self.symb_limit = symb_limit

    def log(self, *args):
        if len(self._label.text) > self.symb_limit:
            self.clear()
        new_text = '>> ' + ' '.join(map(str, args)) + '\n'
        self._label.text += new_text

    def clear(self):
        self._label.text = ''

    def draw(self):
        self._label.draw()


class Client:
    def __init__(self, world):
        self.world = world
        self._modelview = vec(GLdouble, * [0] * 16)
        self._viewport = vec(GLint, * [0] * 4)
        self._projection = vec(GLdouble, * [0] * 16)
        self.width, self.height = 0, 0
        self.wx, self.wy = 0, 0
        self.gx, self.gy, self.gz = 0, 0, 0

    def set_viewport(self, width, height):
        self.width = width
        self.height = height
        glViewport(0, 0, width, height)
        glGetIntegerv(GL_VIEWPORT, self._viewport)

    def _set_projection(self, gl_cmd):
        glMatrixMode(GL_PROJECTION)
        glPushMatrix()
        glLoadIdentity()
        gl_cmd()
        glMatrixMode(GL_MODELVIEW)
        glPushMatrix()
        glLoadIdentity()

    def move_mouse(self, x, y):
        self.wx, self.wy = x, y

    def _unset_projection(self):
        glPopMatrix()
        glMatrixMode(GL_PROJECTION)
        glPopMatrix()
        glMatrixMode(GL_MODELVIEW)

    # http://nehe.gamedev.net/article/using_gluunproject/16013/
    def _unproject(self):
        z = vec(GLfloat, 0)
        glReadPixels(self.wx, self.wy, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, z)
        glGetDoublev(GL_PROJECTION_MATRIX, self._projection)
        glGetDoublev(GL_MODELVIEW_MATRIX, self._modelview)
        px, py, pz = vec(GLdouble, 0), vec(GLdouble, 0), vec(GLdouble, 0)
        gluUnProject(self.wx, self.wy, z[0], self._modelview,
                     self._projection, self._viewport, px, py, pz)
        self.gx, self.gy, self.gz = px[0], py[0], pz[0]
        self.world.selected_cell = (int(self.gx), int(self.gy))

    def _project(self, x, y, z):
        px, py, pz = vec(GLdouble, 0), vec(GLdouble, 0), vec(GLdouble, 0)
        gluProject(x, y, z, self._modelview, self._projection,
                   self._viewport, px, py, pz)
        return px[0], py[0], pz[0]

    def draw(self):
        self._set_projection(
            lambda: gluPerspective(40, self.width / self.height, 0.01, 500))
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glPushMatrix()
        self.world.draw()
        self._unproject()
        glPopMatrix()
        self._unset_projection()
        self._set_projection(lambda:
                             glOrtho(0, self.width, self.height, 0, -10, 10))
        for monster in self.world.game.monsters:
            self._draw_monster_health(monster)
        glColor3f(1, 1, 1)
        self._unset_projection()

    def _draw_monster_health(self, monster):
        ratio = monster.hp / monster.max_hp
        if 0 < ratio < 1:
            color = (2 * (1 - ratio), 1, 0) if ratio > 0.5 else (1, 2 * ratio, 0)
            x, y = monster.location
            width = 20 - int(40 * (1 - ratio))
            glPushMatrix()
            x, y, _ = self._project(x + .5, y + .5, 1.5)
            glTranslatef(x, self.height - y, 0)
            glBegin(GL_QUADS)
            glColor3f(0, 0, 0)
            glVertex2i(20, 7)
            glVertex2i(20, 0)
            glVertex2i(width, 0)
            glVertex2i(width, 7)
            glColor3f(*color)
            glVertex2i(-20, 0)
            glVertex2i(-20, 7)
            glVertex2i(20, 7)
            glVertex2i(20, 0)
            glEnd()
            glPopMatrix()

    # todo: lighting and colors
    def init(self):
        glClearColor(196/255, 230/255, 179/255, 1)
        glShadeModel(GL_SMOOTH)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_BLEND)
        glDepthMask(GL_TRUE)
        glDepthRange(0.0, 1.0)
        glEnable(GL_CULL_FACE)
        glColor3f(1, 1, 1)
        glDepthFunc(GL_LESS)
        """
        glEnable(GL_LIGHTING)
        glLightfv(GL_LIGHT0, GL_POSITION, vec(GLfloat, 10, 10, 10, 1))
        glLightfv(GL_LIGHT0, GL_DIFFUSE, vec(GLfloat, 1, 1, 1, 1))
        glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1)
        glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05)
        glEnable(GL_LIGHT0)
        """
