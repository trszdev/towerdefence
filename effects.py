from entities import Effect

# TODO
class FrostEffect(Effect):
    def __init__(self, duration, value, is_abs):
        super().__init__(duration)
        self.value = value
        self.is_abs = is_abs

    def tick(self, dt):
        pass


# could be a poison/decay effect
class DamageEffect(Effect):
    def __init__(self, value, is_abs, duration=0.0):
        super().__init__(duration)
        self.value = value
        self.is_abs = is_abs

    def tick(self, dt):
        val = self.value * self.aim.hp if self.is_abs else self.value
        self.aim.hp -= (dt + 1) * val
        super().tick(dt)
