![Безымянный.png](https://bitbucket.org/repo/7Ezz5yz/images/2661875128-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)
# Tower defence (python3)

## Installation
```cmd
pip install pyglet
pip install Pillow
```

## Run
```cmd
cd towerdefence
python3 main.py
```

## Tests
```cmd
python3 tests.py
```