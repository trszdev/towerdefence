import unittest
import math2
import entities
import game


class Math2(unittest.TestCase):
    def test_euclidean(self):
        self.assertEquals(math2.euclidean((1,), (-66,)), 67)
        self.assertEquals(math2.euclidean((1,), (54,)), 53)
        self.assertEquals(math2.euclidean((3, 3), (9, 3)), 6)
        self.assertEquals(math2.euclidean((8, 3), (8, -7)), 10)
        self.assertEquals(math2.euclidean((3, 3, 3), (9, 3, 3)), 6)
        self.assertEquals(math2.euclidean((0, 3), (4, 0)), 5)

    def test_clamp(self):
        self.assertEquals(math2.clamp(3, -1, 1), 1)
        self.assertEquals(math2.clamp(-3, -1, 1), -1)
        self.assertEquals(math2.clamp(0.23, -1, 1), 0.23)
        self.assertEquals(math2.clamp(-0.456232, -1, 1), -0.456232)

    def test_copysign(self):
        self.assertEquals(math2.copysign(0, 123), 0)
        self.assertEquals(math2.copysign(0, -123), 0)
        self.assertEquals(math2.copysign(0, 0.2), 0)
        self.assertEquals(math2.copysign(0, 0), 0)
        self.assertEquals(math2.copysign(123, 1), 123)
        self.assertEquals(math2.copysign(123, -1), -123)
        self.assertEquals(math2.copysign(123, 1e-30, eps=1e-5), 0)
        self.assertEquals(math2.copysign(123, 0), 0)

    def test_is_in_segment(self):
        self.assertEquals(math2.is_in_segment((3,), ((-9,), (-8,))), False)
        self.assertEquals(math2.is_in_segment((3,), ((-8,), (3,))), True)
        self.assertEquals(math2.is_in_segment((3,), ((3,), (5,))), True)
        self.assertEquals(math2.is_in_segment((3,), ((2,), (4,))), True)
        self.assertEquals(math2.is_in_segment((5, 5), ((-1, -1), (6, 6))), True)
        self.assertEquals(math2.is_in_segment((5, 5), ((7, 7), (6, 6))), False)

    def test_manhattan(self):
        self.assertEquals(math2.manhattan((1,), (-66,)), 67)
        self.assertEquals(math2.manhattan((1,), (54,)), 53)
        self.assertEquals(math2.manhattan((3, 3), (9, 3)), 6)
        self.assertEquals(math2.manhattan((8, 3), (8, -7)), 10)
        self.assertEquals(math2.manhattan((3, 3, 3), (9, 3, 3)), 6)
        self.assertEquals(math2.manhattan((0, 3), (4, 0)), 4)


class Entities(unittest.TestCase):
    def test_empty_ents(self):
        ents = entities.EntFactory()
        self.assertEquals([*ents.get_entities()], [])
        self.assertEquals(ents.produced, 0)

    def test_builtin_ents(self):
        ents = entities.EntFactory()
        to_add = [entities.Missile(30), entities.Monster(100, 10), entities.Tower(10, 0, 10)]
        types = [*map(lambda x: x.type, to_add)]
        for i, x in enumerate(to_add):
            ents.add_entity(x)
            self.assertNotEquals(types[i], x.type)
        types = [*map(lambda x: x.type, to_add)]
        self.assertEquals(len(to_add), len(types))
        self.assertEquals(set(ents.get_entities()), set(to_add))
        for i in range(len(to_add)):
            self.assertEquals(i, ents.produced)
            self.assertEquals(ents.get_entity(types[i]).id, to_add[i].id)
            self.assertNotEquals(ents.create_new(types[i]).id, to_add[i].id)


class Game(unittest.TestCase):
    def get_game(self, route, gold, sell_rate, speed):
        tower = entities.Tower(10, 0, 1)
        ents = entities.EntFactory().add_entity(tower)
        rules = game.Rules([], {tower.type: 10}, ents,
                           route, wave_interval=10, width=20, height=20,
                           gold=gold, sell_rate=sell_rate, speed=speed)
        return game.Game(rules)

    def test_tower_building_no_route(self):
        g = self.get_game([], 10 * 20 * 20, 1, 1)
        tower, *_ = g.rules.ents.get_entities()
        self.assertEquals(g.time, 0)
        self.assertEquals(g.wave, 0)
        for i in range(20):
            for j in range(20):
                pos = (i, j)
                self.assertIsNone(g.tower_at(pos))
                self.assertEquals(g.gold, 10 * 20 * 20)
                self.assertTrue(g.can_build(pos))
                self.assertTrue(g.build_tower(pos, tower.type))
                self.assertEquals(g.tower_at(pos).type, tower.type)
                self.assertNotEquals(g.tower_at(pos).id, tower.id)
                self.assertEquals(g.gold, 10 * 20 * 20 - 10)
                self.assertTrue(g.sell_tower(pos))
                self.assertEquals(g.gold, 10 * 20 * 20)
        self.assertEquals(g.time, 0)
        self.assertEquals(g.wave, 0)

    def test_tower_building_common(self):
        g = self.get_game([(5, 5)], 2 * 10, 0.75, 1)
        tower, *_ = g.rules.ents.get_entities()
        self.assertEquals(g.time, 0)
        self.assertEquals(g.wave, 0)
        self.assertIsNone(g.tower_at((5, 5)))
        self.assertFalse(g.can_build((5, 5)))
        self.assertFalse(g.build_tower((5, 5), tower.type))
        self.assertFalse(g.sell_tower((5, 5)))
        self.assertIsNone(g.tower_at((5, 5)))

        self.assertEquals(g.gold, 20)
        self.assertIsNone(g.tower_at((4, 4)))
        self.assertTrue(g.can_build((4, 4)))
        self.assertTrue(g.build_tower((4, 4), tower.type))
        self.assertFalse(g.can_build((4, 4)))
        self.assertEquals(g.tower_at((4, 4)).type, tower.type)
        self.assertEquals(g.gold, 10)
        self.assertEquals(g.sell_tower((4, 4)), 10 * 0.75)
        self.assertEquals(g.gold, 10 * 1.75)
        self.assertIsNone(g.tower_at((4, 4)))
        self.assertEquals(g.sell_tower((4, 4)), 0)
        self.assertTrue(g.can_build((4, 4)))
